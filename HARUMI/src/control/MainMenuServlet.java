package control;

import static constant.URLConstant.*;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * メインメニュー遷移用サーブレット
 * @author ilearning
 */

@WebServlet("/MainMenu")
public class MainMenuServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		doPost(request, response);
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		// メインメニューへ遷移
		RequestDispatcher dispatcher = request.getRequestDispatcher(MAIN_MENU_URL);
		dispatcher.forward(request, response);
	}

}
