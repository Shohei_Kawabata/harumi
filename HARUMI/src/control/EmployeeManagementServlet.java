package control;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import model.entity.Employee;
import model.service.AuthService;

@WebServlet("/EmployeeManagement")
public class EmployeeManagementServlet extends HttpServlet {

	private static final long serialVersionUID = 1L;
	
	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		response.sendRedirect("/index.jsp");
	}
	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		HttpSession session = request.getSession();
		Employee employee = (Employee) session.getAttribute("LoginUserInfo");
		String path;
		AuthService auth = new AuthService();
		if (auth.isAdmin(employee)) {
			path = "EmpManagement.jsp";
		} else {
			request.setAttribute("msg", "管理者じゃありません、しね");
			path = "MainMenu.jsp";
		}
		request.getRequestDispatcher(path).forward(request, response);
	}
}
