package control;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import model.service.EmployeeManager;

@WebServlet("/EmployeeDelete")
public class EmployeeDeleteServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		doPost(req, resp);
	}
	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		EmployeeManager empMana = new EmployeeManager();
		String[] inputDeleteId = request.getParameterValues("deleteId");
		
		if (inputDeleteId != null) {
			int[] deleteId = new int[inputDeleteId.length];
			for (int i = 0; i < inputDeleteId.length; i++) {
				deleteId[i] = Integer.parseInt(inputDeleteId[i]);
			}
			String msg;
			if (empMana.delete(deleteId)) {
				msg = "削除成功";
			} else {
				msg = "削除失敗";
			}
			request.setAttribute("msg", msg);
		}
		request.setAttribute("employees", empMana.getEmployees());
		request.getRequestDispatcher("EmpDelete.jsp").forward(request, response);
	}
}
