package control;

import static constant.OrderConstant.*;
import static constant.URLConstant.*;

import java.io.IOException;
import java.util.List;
import java.util.Map;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import constant.RemainLevel;
import model.entity.Product;
import model.entity.Stock;
import model.service.OrderService;
import model.service.StockManager;

/**
 * 注文受付画面 遷移用サーブレット
 * @author ilearning
 */

@WebServlet("/Order")
public class OrderServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		doPost(request, response);
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		// コーヒーの残量を取得
		StockManager stockManager = new StockManager();
		Stock stockData = stockManager.getStock();
		request.setAttribute("stockQuantity", stockData.getQuantity());

		RemainLevel level = stockManager.getRemainLevel(stockData);
		request.setAttribute("orderStockLevel", level);
		// 10Lを切っている場合は注文受付画面に注文受付停止の値を送る
		switch (level) {
		case DANGER:
			request.setAttribute("orderAttentionMessage", ORDER_STOP_MESSAGE);
			break;
		// 15Lを切っている場合は注文受付画面に残量低下を喚起するための値を送る
		case WARNING :
			request.setAttribute("orderAttentionMessage", ORDER_LOW_MESSAGE);
			break;
		default:
		}

		// 商品情報一覧を取得
		OrderService orderService = new OrderService();
		List<Product> productsList = orderService.getProducts();
		request.setAttribute("productsList", productsList);

		// 残量から提供可能な商品情報の数を取得
		Map<Product, Integer> productsMax = stockManager.maxPurchase();
		request.setAttribute("productsMax", productsMax);

		RequestDispatcher dispatcher = request.getRequestDispatcher(OREDER_URL);
		dispatcher.forward(request, response);
	}

}
