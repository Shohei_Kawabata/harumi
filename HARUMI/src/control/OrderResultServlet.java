package control;

import static constant.URLConstant.*;

import java.io.IOException;
import java.util.Map;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import constant.ReceiptConstant;
import model.entity.Employee;
import model.entity.Sale;
import model.service.OrderService;
import model.service.SaleService;

/**
 * 注文完了画面 遷移用サーブレット
 * @author ilearning
 */

@WebServlet("/Result")
public class OrderResultServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		doPost(request, response);
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		HttpSession session = request.getSession();

		// 注文結果をDBに保存
		Sale saleResult = (Sale) session.getAttribute("sale");
		saleResult.setId(new OrderService().commitSale(saleResult));

		// 金種結果をセット
		Map<Integer, Integer> moneyType = new SaleService()
				.changeMoney(Integer.parseInt(request.getParameter("change")));
		session.setAttribute("moneyType", moneyType);

		// レシート 固定文字列（住所等）をセット
		for (ReceiptConstant constant : ReceiptConstant.values()) {
			session.setAttribute(constant.name(), constant.getLabel());
		}
		SaleService ss = new SaleService();
		Sale zikan = ss.getZikan(saleResult.getId());

		// 曜日 注文日時 をセット
		session.setAttribute("saleTime", zikan.getDayOfTheWeekShort());
		session.setAttribute("saleTime", zikan.getSaleTime());

		// レジ担当者名をセット
		Employee loginUserInfo = (Employee) session.getAttribute("LoginUserInfo");
		session.setAttribute("userFirstName", loginUserInfo.getFirstName());
		session.setAttribute("userLastName", loginUserInfo.getLastName());

		// 預かり金額、お釣りをセット
		session.setAttribute("deposit", request.getParameter("deposit"));
		session.setAttribute("change", request.getParameter("change"));

		RequestDispatcher dispatcher = request.getRequestDispatcher(RESULT_URL);
		dispatcher.forward(request, response);
	}

}
