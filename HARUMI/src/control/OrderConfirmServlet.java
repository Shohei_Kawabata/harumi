package control;

import static constant.URLConstant.*;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import model.entity.Employee;
import model.entity.Order;
import model.entity.Product;
import model.entity.Sale;
import model.service.OrderService;

/**
 * 注文確認画面 遷移用サーブレット
 * @author ilearning
 */

@WebServlet("/Confirm")
public class OrderConfirmServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		doPost(request, response);
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		HttpSession session = request.getSession();
		Employee emp = (Employee)session.getAttribute("LoginUserInfo");

		OrderService os = new OrderService();
		List<Product> productList = os.getProducts();	// 既存の商品一覧取得
		List<Order> orderList = new ArrayList<>();		// 商品一個あたりの注文のリスト準備
		for (int i = 0; i < productList.size(); i ++) {
			String[] params = request.getParameterValues(String.valueOf(productList.get(i).getId())); // productIdでparameterたちを取得
			Order o = os.createOrder(productList.get(i).getId(), Integer.parseInt(params[0]));		// productIdと個数でオーダーを作成
			orderList.add(o);
		}
		orderList.removeAll(Collections.singleton(null)); // ぬるぽよをリストから全削除
		session.setAttribute("orders", orderList);
		// 小計から税 + 合計金額を求める
		Sale sale = os.getSale(emp, orderList);
		session.setAttribute("sale", sale);

		RequestDispatcher dispatcher = request.getRequestDispatcher(OREDER_COMFIRM_URL);
		dispatcher.forward(request, response);
	}

}
