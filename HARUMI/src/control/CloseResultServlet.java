package control;

import static constant.CloseConstant.*;
import static constant.URLConstant.*;

import java.io.IOException;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import model.entity.ProductSaleInfo;
import model.entity.Sale;
import model.entity.Stock;
import model.service.SaleService;
import model.service.StockManager;

/**
 * 閉店処理完了画面 遷移用サーブレット
 * @author ilearning
 */

@WebServlet("/CloseResult")
public class CloseResultServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		doPost(request, response);
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		StockManager stockManager = new StockManager();

		// タンク残量を取得
		Stock stockData = stockManager.getStock();
		request.setAttribute("stockQuantity", stockData.getQuantity());

		// タンク情報を初期化
		boolean closeResult = stockManager.close();

		// 初期化に失敗した場合はエラーメッセージを保持し、閉店処理画面へ遷移
		if (!closeResult) {
			request.setAttribute("msg", CLOSE_ERROR_MESSAGE);
			RequestDispatcher dispatcher = request.getRequestDispatcher(CLOSE_URL);
			dispatcher.forward(request, response);

		} else {
			SaleService saleService = new SaleService();

			// 一日の売上情報を取得
			Sale totalSale = saleService.totalSale();
			request.setAttribute("totalSale", totalSale);

			// 商品別の売上情報を取得
			List<ProductSaleInfo> productTotals = saleService.productTotals();
			request.setAttribute("productTotals", productTotals);

			RequestDispatcher dispatcher = request.getRequestDispatcher(CLOSE_RESULT_URL);
			dispatcher.forward(request, response);
		}
	}

}
