package control;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.fasterxml.jackson.databind.ObjectMapper;

import model.entity.Stock;
import model.service.StockManager;

/**
 * Servlet implementation class TestOrderServlet
 */
@WebServlet("/TestOrderServlet")
public class TestOrderServlet extends HttpServlet {

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		try {
			// コーヒーの残量を取得
			StockManager stockManager = new StockManager();
			Stock stockData = stockManager.getStock();
			request.setAttribute("stockQuantity", stockData.getQuantity());

	        //JSONマップ
	        Map<String, String> mapMsg = new HashMap<String, String>();

	        //追加
	        mapMsg.put("response1", String.valueOf(stockData.getQuantity()));

	        // マッパ(JSON <-> Map, List)
	        // JavaオブジェクトからJSON形式に変換
	        // ObjectMapperの利用はライブラリの追加必須
	        ObjectMapper mapper = new ObjectMapper();
	        String jsonStr = mapper.writeValueAsString(mapMsg);  //list, map
	        System.out.println(jsonStr);

	        //ヘッダ設定
	        response.setContentType("application/json;charset=UTF-8");   //JSON形式, UTF-8

	        //pwオブジェクト
	        PrintWriter pw = response.getWriter();

	        //出力
	        pw.print(jsonStr);

	        //クローズ
	        pw.close();

		} catch (Exception e) {
			 e.printStackTrace();
		}


	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
