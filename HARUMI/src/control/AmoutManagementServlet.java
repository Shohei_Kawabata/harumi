package control;

import static constant.AmountConstant.*;
import static constant.URLConstant.*;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import model.entity.Stock;
import model.service.StockManager;

/**
 * コーヒー管理画面 遷移用サーブレット
 * @author ilearning
 */

@WebServlet("/AmoutManagement")
public class AmoutManagementServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		doPost(request, response);
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		StockManager stockManager = new StockManager();
		int amountFlag = Integer.parseInt(request.getParameter("amountFlag"));

		// コーヒー管理画面から遷移した場合、実行
		if( amountFlag == AMOUNT_CHANGE_FLAG ) {

			int changeAmount = Integer.parseInt(request.getParameter("changeAmount"));

			// 45ℓ以上だった場合はエラーメッセージを挿入
			if (changeAmount > 45000) {
				request.setAttribute("msg", CHANGE_MAX_MESSAGE);

			// 変更量に伴いDB更新し、変更完了メッセージを入れる
			} else {
				stockManager.update(changeAmount, stockManager.getStock());
				request.setAttribute("changeResultMessage", CHANGE_RESULT_MESSAGE);
			}
		}

		// コーヒーの残量を取得
		Stock stockData = stockManager.getStock();
		request.setAttribute("stockQuantity", stockData.getQuantity());

		RequestDispatcher dispatcher = request.getRequestDispatcher(AMOUT_MANAGEMENT_URL);
		dispatcher.forward(request, response);
	}

}
