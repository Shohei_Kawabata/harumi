package control;

import static constant.LoginConstant.*;
import static constant.URLConstant.*;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import model.entity.Employee;
import model.service.AuthService;

/**
 * ログイン用サーブレット
 * @author ilearning
 */

@WebServlet("/Login")
public class LoginServlet extends HttpServlet {

	private static final long serialVersionUID = 1L;

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		// ログイン画面へリダイレクト
		response.sendRedirect("./" + LOGIN_URL);
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		Employee emp = new Employee();

		emp.setLoginName(request.getParameter("user-id"));
		emp.setPassword(request.getParameter("password"));

		// ログイン可能なユーザー情報があるか確認
		AuthService auth = new AuthService();
		Employee empResult = auth.login(emp);

		// ユーザー情報がない場合はログイン画面へ遷移
		if (empResult == null) {

			// ログインエラーメッセージをセット
			request.setAttribute("msg", LOGIN_ERROR_MESSAGE);

			RequestDispatcher dispatcher = request.getRequestDispatcher(LOGIN_URL);
			dispatcher.forward(request, response);
		} else {

			// ユーザー情報がある場合はメニュー画面へ遷移
			HttpSession session = request.getSession();
			session.setAttribute("LoginUserInfo", empResult);

			RequestDispatcher dispatcher = request.getRequestDispatcher(MAIN_MENU_URL);
			dispatcher.forward(request, response);
		}
	}
}
