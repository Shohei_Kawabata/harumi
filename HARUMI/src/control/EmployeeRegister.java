package control;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import model.entity.Employee;
import model.service.EmployeeManager;

@WebServlet("/EmployeeRegister")
public class EmployeeRegister extends HttpServlet {
	private static final long serialVersionUID = 1L;

	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.setCharacterEncoding("UTF-8");
		String[] paramNames = {"lastName", "firstName", "loginName", "password"};
		String[] params = new String[paramNames.length];
		boolean isNull = false;
		for (int i = 0; i < params.length; i++) 	{
			params[i] = request.getParameter(paramNames[i]);
			if (params[i] == null) {
				isNull = true;
				break;
			}
		}
		if (!isNull) {
			Employee emp = new Employee();
			emp.setLastName(params[0]);
			emp.setFirstName(params[1]);
			emp.setLoginName(params[2]);
			emp.setPassword(params[3]);
			String msg;
			if (new EmployeeManager().register(emp)) {
				msg = "登録しました";
			} else {
				msg = "登録失敗";
			}
			request.setAttribute("msg", msg);
		} 
		request.getRequestDispatcher("EmpRegister.jsp").forward(request, response);
	}
}
