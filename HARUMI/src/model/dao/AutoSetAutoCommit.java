package model.dao;

import java.sql.Connection;
import java.sql.SQLException;

public class AutoSetAutoCommit implements AutoCloseable {
	private Connection con;
    private boolean beforeAutoCommit;

    public AutoSetAutoCommit(Connection con, boolean autoCommit) throws SQLException {
        this.con = con;
        beforeAutoCommit = con.getAutoCommit();
        con.setAutoCommit(autoCommit);
    }

    @Override
    public void close() throws SQLException {
        con.setAutoCommit(beforeAutoCommit);
    }
}
