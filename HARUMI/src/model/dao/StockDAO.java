package model.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import model.entity.Stock;

public class StockDAO {

	private static final String SELECT = "SELECT stock_id, stock_quantity"
			+ " , register_time FROM stocks WHERE register_time LIKE current_date || '%'"
			+ " ORDER BY register_time DESC";
	private static final String UPDATE = "UPDATE stocks"
			+ " SET stock_quantity = ? "
			+ " WHERE stock_id = ?";
	private static final String INSERT = "INSERT INTO stock VALUES(DEFAULT, "
			+ "?, CURRENT_TIMESTAMP)";
	/**
	 * 今日の日付のストックを返す
	 * @param con
	 * @return
	 * @throws SQLException
	 */
	public Stock todayStock(Connection con) throws SQLException {
		try (PreparedStatement ps = con.prepareStatement(SELECT);
			 ResultSet rs = ps.executeQuery()) {
			rs.next();
			Stock stock = new Stock();
			stock.setId(rs.getInt(1));
			stock.setQuantity(rs.getInt(2));
			stock.setRegisterTime(rs.getTimestamp(3));
			return stock;
		}
	}
	/**
	 * ストックの更新(stock_quantity) しかも今日の文
	 * @param con
	 * @param capa
	 * @param stock
	 * @return
	 * @throws SQLException
	 */
	public boolean update(Connection con, int capa, Stock stock) throws SQLException {
		try (PreparedStatement ps = con.prepareStatement(UPDATE)) {
			ps.setInt(1, capa);
			ps.setInt(2, stock.getId());
			ps.executeUpdate();
			return true;
		}
	}
	/**
	 * 開店用
	 * @param con
	 * @return
	 * @throws SQLException
	 */
	public Stock insert(Connection con) throws SQLException {
		final int DEFAULT_CAPA = 50000;
		try (PreparedStatement ps = con.prepareStatement(INSERT)) {
			ps.setInt(1, DEFAULT_CAPA);
			ps.executeQuery();
			return todayStock(con);
		}
	}
	/**
	 * 閉店
	 */
	public boolean close(Connection con) throws SQLException {
		return update(con, 0, todayStock(con));
	}

}
