package model.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import model.entity.Order;

public class OrderDAO {
	private static final String INSERT = "INSERT INTO orders VALUES(DEFAULT, ?, ?, ?, ?)";
	public Order insert(Connection con, Order order, int saleId) throws SQLException {
		try (PreparedStatement ps = con.prepareStatement(INSERT,Statement.RETURN_GENERATED_KEYS)) {
			ps.setInt(1, saleId);
			ps.setInt(2, order.getProduct().getId());
			ps.setInt(3, order.getQuantity());
			ps.setInt(4, order.getTotalPrice());
			if (ps.executeUpdate() <= 0) {
				throw new SQLException("INSERT失敗");
			}
			try (ResultSet rs = ps.getGeneratedKeys()) {
				rs.next();
				order.setId(rs.getInt(1));
				return order;
			}
		}
	}
}
