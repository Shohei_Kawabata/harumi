package model.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import model.entity.ProductSaleInfo;
import model.entity.Sale;

public class SaleDAO {
	private static String INSERT = "INSERT INTO sales VALUES(DEFAULT, ?, ?,CURRENT TIMESTAMP)";
	private static String TOTAL = "SELECT COUNT(*) AS count, SUM(sale_price) AS total FROM sales"
			+ " WHERE sale_time LIKE current_date || '%' ORDER BY sale_time DESC";
	private static String PRODUCT_TOTAL = "SELECT COUNT(*) AS count, SUM(sale_price) AS total, product_name FROM sales INNER JOIN orders "
			+ "ON sales.sale_id = orders.sale_id INNER JOIN products ON orders.product_id = products.product_id"
			+ " WHERE (sale_time LIKE current_date || '%' ORDER BY sale_time DESC) AND sales.product_id = ?"
			+ " GROUP BY product_name";
	private static String SELECT = "SELECT * FROM sales WHERE sale_id = ?";
	public int insert(Connection con, Sale sale) throws SQLException {
		try (PreparedStatement ps = con.prepareStatement(INSERT, Statement.RETURN_GENERATED_KEYS)) {
			ps.setInt(1, sale.getEmployee().getId());
			ps.setInt(2, sale.getSalePrice());
			if (ps.executeUpdate() <= 0) {
				throw new SQLException("sales INSERT failuer");
			}
			try (ResultSet rs = ps.getGeneratedKeys()) {
				if (rs.next()) {
					return rs.getInt(1);
				} else {
					throw new SQLException("Sales INSERT failuer");
				}
			}
		}
	}
	public Sale total(Connection con) {
		try (PreparedStatement ps = con.prepareStatement(TOTAL);
			 ResultSet rs = ps.executeQuery()) {
			if (rs.next()) {
				Sale sale = new Sale();
				sale.setSalePrice(rs.getInt(2));
				return sale;
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return null;
	}
	public ProductSaleInfo productTotal(Connection con, int productId) {
		try (PreparedStatement ps = con.prepareStatement(PRODUCT_TOTAL)) {
			ps.setInt(1, productId);
			try (ResultSet rs = ps.executeQuery()) {
				if (rs.next()) {
					ProductSaleInfo psi = new ProductSaleInfo();
					psi.setCount(rs.getInt(1));
					psi.setPrice(rs.getInt(2));
					psi.setProductName(rs.getString(3));
					return psi;
				}
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return null;
	}
	public Sale getSale(Connection con,int id) throws SQLException {
		try (PreparedStatement ps = con.prepareStatement(SELECT)) {
			ps.setInt(1, id);
			try (ResultSet rs = ps.executeQuery()) {
				rs.next();
				Sale sale = new Sale();
				sale.setSaleTime(rs.getTimestamp("sale_time"));
				return sale;
			}
		}
	}
}
