package model.dao;

import java.sql.Connection;
import java.sql.SQLException;

public class AutoRollback implements AutoCloseable {
	private Connection con;
	private boolean committed;

	public AutoRollback(Connection con) throws SQLException {
		this.con = con;        
	}

	public void commit() throws SQLException {
		con.commit();
		committed = true;
	}

	@Override
	public void close() throws SQLException {
		if (!committed) {
			con.rollback();
		}
	}

}
