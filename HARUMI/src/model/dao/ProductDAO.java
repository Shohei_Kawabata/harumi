package model.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import model.entity.Order;
import model.entity.Product;

public class ProductDAO {

	private static final String SELECT_ALL = "SELECT product_id, product_name, "
			+ "capacity, price FROM products ";
	private static final String SELECT_ID = "SELECT product_id, product_name, "
			+ "capacity, price FROM products WHERE product_id = ?";
	public List<Product> findAll(Connection con) throws SQLException{
		List<Product> products = new ArrayList<>();
		try (PreparedStatement ps = con.prepareStatement(SELECT_ALL);
				ResultSet rs = ps.executeQuery()) {
			while (rs.next()) {
				products.add(prepareProduct(rs));
			}
			return products;
		}
	}
	public Product findById(Connection con, Product product) throws SQLException {
		try (PreparedStatement ps = con.prepareStatement(SELECT_ID)) {
			ps.setInt(1, product.getId());
			try (ResultSet rs = ps.executeQuery()) {
				if (rs.next()) {
					return prepareProduct(rs);
				}
			}
		}
		return null;
	}
	public int getTotalPrice(Connection con, Order order) throws SQLException {
		Product product = findById(con, order.getProduct());
		if (product == null) {
			System.out.println("存在しないProductIdです");
			throw new SQLException("存在しないProductIdです");
		}
		return product.getPrice() * order.getQuantity();
	}
	
	private Product prepareProduct(ResultSet rs) throws SQLException {
		Product product = new Product();
		product.setId(rs.getInt(1));
		product.setName(rs.getString(2));
		product.setCapacity(rs.getInt(3));
		product.setPrice(rs.getInt(4));
		return product;
	}
} 
