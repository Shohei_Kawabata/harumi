package model.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import model.entity.Employee;

public class EmployeeDAO {
	private static final String SELECT_ALL = "SELECT employee_id, last_name, first_name,"
			+ "  login_id, login_password, admin_flag FROM employees";
	private static final String SELECT_EMP = "SELECT employee_id, last_name, first_name,"
			+ " login_id, login_password, admin_flag FROM employees "
			+ " WHERE login_id = ? AND login_password = ?";
	private static final String SELECT_NAME = "SELECT login_id FROM employees WHERE login_id = ?";
	private static final String SELECT_ID = "SELECT employee_id, last_name, first_name,"
			+ " login_id, login_password, admin_flag FROM employees WHERE employee_id = ?";
	private static final String INSERT = "INSERT INTO employees VALUES(DEFAULT, ?, ?, ?, ?, 0)";
	private static final String DELETE = "DELETE FROM employees WHERE employee_id = ?";
	
	/**
	 * 従業員データの全件取得
	 * @param con
	 * @return
	 * @throws SQLException
	 */
	public List<Employee> findAll(Connection con) throws SQLException {
		List<Employee> employees = new ArrayList<>();
		try (PreparedStatement ps = con.prepareStatement(SELECT_ALL);
			 ResultSet rs = ps.executeQuery()) {
			while (rs.next()) {
				Employee emp = prepareEmployee(rs);
				employees.add(emp);
			}
		}
		if (employees.isEmpty()) return null;
		return employees;
	}
	/**
	 * 従業員のログインID, パスワードの照合を行う
	 * @param con
	 * @param e
	 * @return
	 * @throws SQLException
	 */
	public Employee findByNameAndPass(Connection con, Employee e) throws SQLException {
		if (!loginNameCheck(e) || e.getPassword() == null || e.getPassword().isEmpty()) {
			System.out.println("Employeeがnullまたは情報がたりません");
			return null;
		}
		e.setPassword(toHash(e.getPassword()));		// パスワードをハッシュ化
		try (PreparedStatement ps = con.prepareStatement(SELECT_EMP)) {
			ps.setString(1, e.getLoginName());
			ps.setString(2, e.getPassword());
			try (ResultSet rs = ps.executeQuery()) {
				if (rs.next()) {
					return prepareEmployee(rs);
				}
			}
		}
		return null;
	}
	/**
	 * 従業員IDで検索
	 * @param con
	 * @param emp
	 * @return
	 * @throws SQLException
	 */
	public Employee findById(Connection con, Employee emp) throws SQLException {
		if (emp == null || emp.getId() == 0) {
			System.out.println("Employeeがnullまたは情報がたりません");
			return null;
		}
		try (PreparedStatement ps = con.prepareStatement(SELECT_ID)) {
			ps.setInt(1, emp.getId());
			try (ResultSet rs = ps.executeQuery();) {
				if (rs.next()) {
					return prepareEmployee(rs);
				}
			}
		}
		return null;
	}
	/**
	 * ログインIDの重複があるかチェックする
	 * @param con
	 * @param emp
	 * @return
	 * @throws SQLException
	 */
	public boolean checkDuplication(Connection con, Employee emp) throws SQLException {
		if (!loginNameCheck(emp)) {
			System.out.println("Employeeがnullまたは情報がたりません");
			return false;
		}
		try (PreparedStatement ps = con.prepareStatement(SELECT_NAME)) {
			ps.setString(1, emp.getLoginName());
			try (ResultSet rs = ps.executeQuery()) {
				return !rs.next();
			}
		}
	}
	/**
	 * 従業員の追加
	 * @param con
	 * @param emp
	 * @return
	 * @throws SQLException
	 */
	public boolean addEmployee(Connection con, Employee emp) throws SQLException {
		if (!empCheck(emp)) {
			System.out.println("Employeeがnullまたは情報がたりません");
			return false;
		}
		try (PreparedStatement ps = con.prepareStatement(INSERT)) {
			ps.setString(1, emp.getLastName());
			ps.setString(2, emp.getFirstName());
			ps.setString(3, emp.getLoginName());
			ps.setString(4, toHash(emp.getPassword()));
			if (ps.executeUpdate() <= 0) return false;	// INSERT失敗時はfalse
		}
		return true;
	}
	/**
	 * 従業員ID で削除
	 * @param con
	 * @param emp
	 * @return
	 * @throws SQLException
	 */
	public boolean delete(Connection con, Employee emp) throws SQLException {
		if (emp == null || emp.getId() == 0) {
			System.out.println("Employeeがnullまたは情報がたりません");
			return false;
		}
		try (PreparedStatement ps = con.prepareStatement(DELETE)) {
			ps.setInt(1, emp.getId());
			return ps.executeUpdate() >= 1;
		}
	}

	/**
	 * EmployeeBean の Null チェック
	 * @param emp
	 * @return 問題なければtrue
	 */
	private boolean empCheck(Employee emp) {
		return emp != null &&
				emp.getLastName() != null && !emp.getLastName().isEmpty() &&
				emp.getFirstName() != null && !emp.getFirstName().isEmpty() &&
				loginNameCheck(emp);
	}
	private boolean loginNameCheck(Employee emp) {
		return emp != null && emp.getLoginName() != null && !emp.getLoginName().isEmpty(); 
	}
	/**
	 * パスワードをハッシュ化(のちにちゃんとやる)
	 * @param password
	 * @return
	 */
	private String toHash(String password) {
		return String.valueOf(Objects.hash(password));
	}
	/**
	 * 従業員データ作成用
	 * @param rs
	 * @return
	 * @throws SQLException
	 */
	private Employee prepareEmployee(ResultSet rs) throws SQLException {
		Employee emp = new Employee();
		emp.setId(rs.getInt(1));
		emp.setLastName(rs.getString(2));
		emp.setFirstName(rs.getString(3));
		emp.setLoginName(rs.getString(4));
		emp.setPassword(rs.getString(5));
		emp.setAdmin(rs.getBoolean(6));
		return emp;
	}
}
