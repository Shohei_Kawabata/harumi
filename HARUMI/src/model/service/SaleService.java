package model.service;

import static model.dao.ConnectionManager.*;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import model.dao.ProductDAO;
import model.dao.SaleDAO;
import model.entity.Product;
import model.entity.ProductSaleInfo;
import model.entity.Sale;
public class SaleService {

	/**
	 * 金種計算を行うやつ
	 * @param money
	 * @return Map<金種, 枚数>
	 */
	public Map<Integer, Integer> changeMoney(int money) {
		Map<Integer, Integer> map = new HashMap<>();
		int[] kinds = { 10000, 5000, 1000, 500, 100, 50, 10, 5, 1};
		for (int kind : kinds) {
			int count = money / kind;
			money %= kind;
			map.put(kind, count);
		}
		return map;
	}
	public Sale totalSale() {
		SaleDAO dao = new SaleDAO();
		try (Connection con = getConnection()) {
			return dao.total(con);
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return null;
	}
	public Sale getZikan(int id) {
		try (Connection con = getConnection()) {
			return new SaleDAO().getSale(con, id);
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return null;
	}
	/**
	 * 商品ごとの売り上げ情報を取得するメソッド
	 * @return productSaleInfo(名前と個数と売上高が入ってる)のリスト
	 */
	public List<ProductSaleInfo> productTotals() {
		SaleDAO dao = new SaleDAO();
		ProductDAO pdao = new ProductDAO();
		try (Connection con = getConnection()) {
			List<Product> pList = pdao.findAll(con);
			List<ProductSaleInfo> infoList = new ArrayList<>();
			for (Product p : pList) {
				ProductSaleInfo pinfo = dao.productTotal(con, p.getId());
				infoList.add(pinfo);
			}
			return infoList;
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return null;
	}

	public int totalSales() {
		throw new RuntimeException("これから実装");
	}
	public String[] groupSales() {
		throw new RuntimeException("これから実装");
	}
}
