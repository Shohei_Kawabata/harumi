package model.service;

import static model.dao.ConnectionManager.*;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import model.dao.AutoRollback;
import model.dao.AutoSetAutoCommit;
import model.dao.OrderDAO;
import model.dao.ProductDAO;
import model.dao.SaleDAO;
import model.dao.StockDAO;
import model.entity.Employee;
import model.entity.Order;
import model.entity.Product;
import model.entity.Sale;
import model.entity.Stock;

public class OrderService {
	public Order createOrder(int productId, int quantity) {
		if (quantity == 0) return null;
		ProductDAO dao = new ProductDAO();
		try (Connection con = getConnection();
				AutoRollback transactionController = new AutoRollback(con);
				AutoSetAutoCommit modeController = new AutoSetAutoCommit(con, false)) {
			Product p = new Product();
			p.setId(productId);
			p = dao.findById(con, p);
			if (p == null) return null;
			Order order = new Order();
			order.setProduct(p);
			order.setQuantity(quantity);
			order.setTotalPrice(p.getPrice() * quantity);
			return order;
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return null;
	}
	/**
	 * 商品一覧を返す
	 * @return
	 */
	public List<Product> getProducts() {
		ProductDAO dao = new ProductDAO();
		try (Connection con = getConnection();
			AutoRollback transactionController = new AutoRollback(con);
			AutoSetAutoCommit modeController = new AutoSetAutoCommit(con, false)) {

			return dao.findAll(con);
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return null;
	}
	/**
	 * 小計を取得 + Saleインスタンス
	 * @param emp empのidが必要
	 * @param orders orderクラスのフィールドproductが最低限必要
	 * @return saleインスタンス(情報フルフル) または null
	 */
	public Sale getSale(Employee emp, Order... orders) {
		ProductDAO productDao = new ProductDAO();
		try (Connection con = getConnection();
			AutoRollback transactionController = new AutoRollback(con);
			AutoSetAutoCommit modeController = new AutoSetAutoCommit(con, false)) {

			int salePrice = 0;
			for (Order o : orders) {
				int totalPrice = productDao.getTotalPrice(con,o);
				o.setTotalPrice(totalPrice);
				salePrice += totalPrice;
			}
			Sale sale = new Sale();
			sale.setEmployee(emp);
			sale.setOrders(new ArrayList<Order>(Arrays.asList(orders)));
			sale.setSalePrice(salePrice);
			return sale;
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return null;
	}
	public Sale getSale(Employee emp, List<Order> orders) {
		return getSale(emp, (Order[])orders.toArray(new Order[0]));
	}
	/**
	 * sales, orders テーブルに登録 + stockテーブル も 更新
	 * @param sale
	 * @return
	 */
	public int commitSale(Sale sale) {
		SaleDAO saleDAO = new SaleDAO();
		OrderDAO orderDAO = new OrderDAO();
		StockDAO stockDAO = new StockDAO();
		try (Connection con = getConnection();
			AutoRollback transactionController = new AutoRollback(con);
			AutoSetAutoCommit modeController = new AutoSetAutoCommit(con, false)) {

			int newSaleId = saleDAO.insert(con, sale);	// sales 自動生成キー取得
			int totalCapa = 0;
			for (Order order : sale.getOrders()) {
				orderDAO.insert(con, order, newSaleId);
				totalCapa += order.getProduct().getCapacity() * order.getQuantity();
			}
			Stock stock = stockDAO.todayStock(con);

			// 現在stock キャパから order計算キャパをマイナス
			stockDAO.update(con, stock.getQuantity() - totalCapa, stock);
			return newSaleId;
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return 0;
	}
	public int currentAmount() {
		throw new RuntimeException("これから実装");
	}

}
