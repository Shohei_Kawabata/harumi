package model.service;

import static model.dao.ConnectionManager.*;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import constant.RemainLevel;
import model.dao.ProductDAO;
import model.dao.StockDAO;
import model.entity.Product;
import model.entity.Stock;

public class StockManager {
	
	/**
	 * 現在のストックを返す
	 * @return stock
	 */
	public Stock getStock() {
		try (Connection con = getConnection()) {
			StockDAO dao = new StockDAO();
			return dao.todayStock(con);
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return null;
	}
	/**
	 * 営業開始時に新規ストック作成
	 * @return
	 */
	public Stock open() {
		try (Connection con = getConnection()) {
			StockDAO dao = new StockDAO();
			return dao.insert(con);
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return null;
	}
	/**
	 * ストックから最大の購入個数を算出(複数のものは対応していないよ, JavaScriptで頑張って)
	 * @return
	 */
	public Map<Product, Integer> maxPurchase() {
		Map<Product, Integer> map = new HashMap<>();
		Stock stock = getStock();
		try (Connection con = getConnection()) {
			List<Product> products = new ProductDAO().findAll(con);
			for (Product p : products) {
				map.put(p, stock.getQuantity() / p.getCapacity());
			}
			return map;
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return null;
	}
	/**
	 * ストックの残量変更
	 * 減らす場合はキャパにマイナスの
	 * @param capa
	 * @param stock
	 * @return
	 */
	public Stock update(int capa, Stock stock) {
		StockDAO dao = new StockDAO();
		try (Connection con = getConnection()) {
			if (dao.update(con, capa + stock.getQuantity(), stock)) {
				return dao.todayStock(con);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return null;
	}
	/**
	 * 
	 * @return
	 */
	public boolean close() {
		StockDAO dao = new StockDAO();
		try (Connection con = getConnection()) {
			return dao.close(con);
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return false;
	}
	
	public RemainLevel getRemainLevel(Stock stock) {
		if (stock.getQuantity() >= RemainLevel.HIGH.getQuantity()) {
			return RemainLevel.HIGH;
		} else if (stock.getQuantity() >= RemainLevel.MIDDLE.getQuantity()) {
			return RemainLevel.MIDDLE;
		} else if (stock.getQuantity() >= RemainLevel.LOW.getQuantity()) {
			return RemainLevel.LOW;
		} else if (stock.getQuantity() >= RemainLevel.WARNING.getQuantity()) {
			return RemainLevel.WARNING;
		} else if (stock.getQuantity() >= RemainLevel.DANGER.getQuantity()) {
			return RemainLevel.DANGER;
		} else {
			return RemainLevel.THE_END;
		}
	}
	public boolean reduce() {
		throw new RuntimeException("これから実装");
	}
	public boolean append() {
		throw new RuntimeException("これから実装");
	}
}
