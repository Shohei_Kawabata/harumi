package model.service;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;

import static model.dao.ConnectionManager.getConnection;

import model.dao.AutoRollback;
import model.dao.AutoSetAutoCommit;
import model.dao.EmployeeDAO;
import model.entity.Employee;

public class EmployeeManager {
	
	/**
	 * ユーザー登録
	 * @param emp id以外の情報が必要
	 * @return
	 */
	public boolean register(Employee emp) {
		EmployeeDAO dao = new EmployeeDAO();
		try (Connection con = getConnection();
		AutoRollback transactionController = new AutoRollback(con);
		AutoSetAutoCommit modeController = new AutoSetAutoCommit(con, false)) {
			// ユーザー名のチェック
			if (!dao.checkDuplication(con, emp)) {
				throw new SQLException("存在するユーザー名です");
			}
			// 追加の処理
			if (!dao.addEmployee(con, emp)) {
				throw new SQLException("追加に失敗しました");
			}		
			return true;
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return false;
	}
	/**
	 * 削除
	 * @param emp idが必要
	 * @return
	 */
	public boolean delete(Employee emp) {
		EmployeeDAO dao = new EmployeeDAO();
		try (Connection con = getConnection();
		AutoRollback transactionController = new AutoRollback(con);
		AutoSetAutoCommit modeController = new AutoSetAutoCommit(con, false)) {
			if (dao.findById(con, emp) == null) {
				throw new SQLException("存在しません");
			}
			if (!dao.delete(con, emp)) {
				throw new SQLException("削除に失敗しました");
			}
			return true;
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return false;
	}
	/**
	 * idで削除
	 * @param empId
	 * @return
	 */
	public boolean delete(int empId) {
		EmployeeDAO dao = new EmployeeDAO();
		try (Connection con = getConnection();
		AutoRollback transactionController = new AutoRollback(con);
		AutoSetAutoCommit modeController = new AutoSetAutoCommit(con, false)) {
			Employee emp = new Employee();
			emp.setId(empId);
			Employee e = dao.findById(con, emp);
			return delete(e);
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return false;
	}
	public boolean delete(int[] ids) {
		for (int id : ids) {
			if (!delete(id)) {
				return false;
			}
		}
		return true;
	}
	public List<Employee> getEmployees() {
		EmployeeDAO dao = new EmployeeDAO();
		try (Connection con = getConnection();
		AutoRollback transactionController = new AutoRollback(con);
		AutoSetAutoCommit modeController = new AutoSetAutoCommit(con, false)) {
			return dao.findAll(con);
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return null;
	}
	public void update() {
		throw new RuntimeException("これから実装");
	}
}
