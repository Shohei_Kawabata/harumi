package model.service;

import java.sql.Connection;
import java.sql.SQLException;

import static model.dao.ConnectionManager.getConnection;
import model.dao.EmployeeDAO;
import model.entity.Employee;

public class AuthService {
	/**
	 * ログインできたらEmployeeを返す
	 * @param employee
	 * @return
	 */
	public Employee login(Employee employee) {
		EmployeeDAO dao = new EmployeeDAO();
		try (Connection con = getConnection()) {
			return dao.findByNameAndPass(con, employee);
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return null;
	}
	public boolean isAdmin(Employee employee) {
		if (employee == null) return false;
		return employee.isAdmin();
	}
	public boolean logout() {
		throw new RuntimeException("これから実装");
	}
}
