package model.entity;

import java.io.Serializable;
/**
 * 従業員を表すクラス
 * @author daisukesasaki
 * @since 2020.06.24
 */
public class Employee implements Serializable{
	private static final long serialVersionUID = 1L;
	private int id;
	private String lastName;
	private String firstName;
	private String loginName;
	private transient String password;
	private boolean isAdmin;
	public Employee() {}
	public Employee(int id, String lastName, String firstName, 
			String loginName, String password, boolean isAdmin) {
		super();
		this.id = id;
		this.lastName = lastName;
		this.firstName = firstName;
		this.loginName = loginName;
		this.password = password;
		this.isAdmin = isAdmin;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getLastName() {
		return lastName;
	}
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	public String getFirstName() {
		return firstName;
	}
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	public String getLoginName() {
		return loginName;
	}
	public void setLoginName(String loginName) {
		this.loginName = loginName;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public boolean isAdmin() {
		return isAdmin;
	}
	public void setAdmin(boolean isAdmin) {
		this.isAdmin = isAdmin;
	}
	@Override
	public String toString() {
		return "Employee [id=" + id + ", lastName=" + lastName + ", firstName=" + firstName + ", loginName=" + loginName + ", password="
				+ password + ", isAdmin=" + isAdmin + "]";
	}
	
}
