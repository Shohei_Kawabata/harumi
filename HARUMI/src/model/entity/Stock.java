package model.entity;

import java.io.Serializable;
import java.sql.Timestamp;

public class Stock implements Serializable {
	private static final long serialVersionUID = 1L;
	private int id;
	private int quantity;
	private Timestamp registerTime;
	public Stock() {}
	public Stock(int id, int quantity, Timestamp registerTime) {
		super();
		this.id = id;
		this.quantity = quantity;
		this.registerTime = registerTime;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public int getQuantity() {
		return quantity;
	}
	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}
	public Timestamp getRegisterTime() {
		return registerTime;
	}
	public void setRegisterTime(Timestamp registerTime) {
		this.registerTime = registerTime;
	}
	@Override
	public String toString() {
		return "Stock [id=" + id + ", quantity=" + quantity + ", registerTime=" + registerTime + "]";
	}
	
}
