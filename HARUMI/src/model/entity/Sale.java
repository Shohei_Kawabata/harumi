package model.entity;
import java.io.Serializable;
import java.sql.Timestamp;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
public class Sale implements Serializable {
	private static final long serialVersionUID = 1L;
	private static double TAX = 0.1;
	private int id;
	private Employee employee;
	private int salePrice;
	private List<Order> orders;
	private Timestamp saleTime;
	public Sale() {}
	public Sale(int id, Employee employee, int salePrice, Timestamp saleTime) {
		super();
		this.id = id;
		this.employee = employee;
		this.salePrice = salePrice;
		this.saleTime = saleTime;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public Employee getEmployee() {
		return employee;
	}
	public void setEmployee(Employee employee) {
		this.employee = employee;
	}
	public int getSalePrice() {
		return salePrice;
	}
	public void setSalePrice(int salePrice) {
		this.salePrice = salePrice;
	}
	public Timestamp getSaleTime() {
		return saleTime;
	}
	public void setSaleTime(Timestamp saleTime) {
		this.saleTime = saleTime;
	}
	public String getDayOfTheWeekShort() { 
	    Calendar cal = Calendar.getInstance();
	    cal.setTime(new Date(saleTime.getTime()));
	    switch (cal.get(Calendar.DAY_OF_WEEK)) {
	        case Calendar.SUNDAY: return "日";
	        case Calendar.MONDAY: return "月";
	        case Calendar.TUESDAY: return "火";
	        case Calendar.WEDNESDAY: return "水";
	        case Calendar.THURSDAY: return "木";
	        case Calendar.FRIDAY: return "金";
	        case Calendar.SATURDAY: return "土";
	    }
	    throw new IllegalStateException();
	}	
	public List<Order> getOrders() {
		return orders;
	}
	public void setOrders(List<Order> orders) {
		this.orders = orders;
	}
	public int getTaxIncludingPrice(int price) {
		return (int)(price + price * TAX);
	}
	public int getProductCount() {
		int productTotalCount = 0;
		for (Order o : orders) {
			productTotalCount += o.getQuantity();
		}
		return  productTotalCount;
	}
	@Override
	public String toString() {
		return "Sale [id=" + id + ", employee=" + employee + ", salePrice=" + salePrice + ", saleTime=" + saleTime
				+ "]";
	}
	
}
