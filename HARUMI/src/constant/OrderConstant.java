package constant;

/**
 * 注文画面用 定数クラス
 * @author ilearning
 */

public class OrderConstant {

	// privateコンストラクタでインスタンス生成を抑制
	private OrderConstant() {}

	public static final int ORDER_STOP_FLAG = 1;

	public static final String ORDER_STOP_MESSAGE = "コーヒーの残量が足りないため、注文受付を停止しております。";

	public static final int ORDER_LOW_FLAG = 2;

	public static final String ORDER_LOW_MESSAGE = "コーヒーの残量が少ないです。10L以下になった場合は注文受付を停止いたします。";

}
