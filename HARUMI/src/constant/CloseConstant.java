package constant;

/**
 * 閉店処理画面用 定数クラス
 * @author ilearning
 */

public class CloseConstant {

	// privateコンストラクタでインスタンス生成を抑制
	private CloseConstant() {}

	public static final String CLOSE_ERROR_MESSAGE = "閉店処理が失敗致しました。";
}
