package constant;

public enum RemainLevel {
	HIGH(40000,"よゆう"),
	MIDDLE(20000, "まだ大丈夫"),
	LOW(15000, "そろそろやばくなってきた"),
	WARNING(10000, "やばいよやばいよ"),
	DANGER(0, "ヤバすぎ"),
	THE_END(-1, "ゲームオーバー");
	
	private int quantity;
	private String message;
	private RemainLevel(int quantity, String message) {
		this.quantity = quantity;
		this.message = message;
	}
	public int getQuantity() {
		return quantity;
	}
	public String toString() {
		return this.name() + ":" + message;
	}
}
