package constant;

/**
 * コーヒー管理画面用 定数クラス
 * @author ilearning
 */

public class AmountConstant {

	// privateコンストラクタでインスタンス生成を抑制
	private AmountConstant() {}

	public static final int AMOUNT_CHANGE_FLAG = 2;

	public static final String CHANGE_RESULT_MESSAGE = "変更が完了致しました。";

	public static final String CHANGE_MAX_MESSAGE = "変更量は45L以下で入力してください。";

}
