package constant;

/**
 * ログイン画面用 定数クラス
 * @author ilearning
 */

public class LoginConstant {

	// privateコンストラクタでインスタンス生成を抑制
	private LoginConstant() {}

	public static final String LOGIN_ERROR_MESSAGE = "ユーザーID,またはパスワードが違います。";

	public static final String LOGOUT_MESSAGE = "ログアウト致しました。";

}
