package constant;

public enum ReceiptConstant {
	POSTAL_CODE_FIX("〒104-0053"),
	ADDRESS_FIX("東京都中央区晴海晴海オフィスタワーX"),
	PHONE_NUM_FIX("Tel：03-3456-XXXX"),
	COMPANY_FIX("はるみコーヒー株式会社"),
	RECEIPT_FIX("【領収証】"),
	SMALL_FIX("スモール：120ml　120円"),
	TALL_FIX("トール：220ml　190円"),
	BIG_FIX("ビッグ：300ml　250円"),
	TOTAL_FIX("合　計"),
	TAX_FIX("内消費税"),
	RECEIVED_FIX("上記正に領収いたしました"),
	TAKETOTAL_FIX("お預り合計"),
	PAY_BACK_FIX("お釣り合計");

	private String label;

	ReceiptConstant(String label) {
		this.label = label;
	}

	public String getLabel() {
		return this.label;
	}
}
