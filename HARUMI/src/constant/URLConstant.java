package constant;

/**
 * 各画面URL用 定数クラス
 * @author ilearning
 */

public class URLConstant {

	// privateコンストラクタでインスタンス生成を抑制
	private URLConstant() {}

	public static final String LOGIN_URL = "Index.jsp";

	public static final String MAIN_MENU_URL = "MainMenu.jsp";

	public static final String OREDER_URL = "Order.jsp";

	public static final String OREDER_COMFIRM_URL = "OrderComfirm.jsp";

	public static final String RESULT_URL = "Result.jsp";

	public static final String AMOUT_MANAGEMENT_URL = "AmoutManagement.jsp";

	public static final String CLOSE_URL = "Close.jsp";

	public static final String CLOSE_RESULT_URL = "CloseResult.jsp";

	public static final String EMP_MANAGEMENT_URL = "EmpManagement.jsp";

	public static final String EMP_REGISTER_URL = "EmpRegister.jsp";

	public static final String EMP_DELETE_URL = "EmpDelete.jsp";

}
