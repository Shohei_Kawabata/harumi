<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%
String footerUri = request.getServletPath();
%>
<head>
  <link rel="stylesheet" type="text/css" href="./css/footer.css">
</head>
<body>
    <hr>
    <div class="footer">
      <input class="ret" type="submit" name="ret" value="戻る" formaction="MainMenu">
<%
if(footerUri.equals("/OrderComfirm.jsp")){
%>
      <input class="conf" type="submit" value="支払い確定" formaction="Result">
<%
} else {
%>
      <input class="conf" type="submit" value="確認">
<%
}
%>
    </div>
</body>
</html>