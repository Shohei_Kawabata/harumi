<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%
	String msg = (String)request.getAttribute("msg");
	if (msg == null) {
		msg = "";
	}
%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>従業員登録</title>
</head>
<body>
	<p><%= msg %></p>
	<form action="EmployeeRegister" method="post">
		<p>姓<input type="text" name="lastName"></p>
		<p>名<input type="text" name="firstName"></p>
		<p>ログインID<input type="text" name="loginName"></p>
		<p>ログインPASSWORD<input type="text" name="password"></p>
		<p><input type="submit" value="登録"></p>
	</form>
</body>
</html>