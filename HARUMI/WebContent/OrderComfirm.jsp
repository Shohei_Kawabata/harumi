<%@page import="model.entity.Sale"%>
<%@page import="model.entity.Order"%>
<%@page import="java.util.List"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%
	List<Order> orderList = (List<Order>) session.getAttribute("orders");
	Sale sale = (Sale) session.getAttribute("sale");
%>
<!DOCTYPE html>
<html>
<head>
  <meta charset="UTF-8">
  <title>注文確認</title>
  <link rel="stylesheet" type="text/css" href="./css/order.css">
  <script type="text/javascript" src="./js/jquery-3.4.1.min.js"></script>
</head>
<body>
  <form action="Result" method="post">
  <%@ include file="header.jsp" %>
    <div class="grid-container">
      <div class="itemlist">
<%
	for (Order o : orderList){
%>
      <p class="item">
        <span><%= o.getProduct().getName() %></span>
        <span><%= o.getProduct().getPrice() %>円</span>
        <span><%= o.getQuantity() %></span>
      </p>
<%
	}
%>
      </div>
      <div class="result" style="border-left: groove;">
        <p id="subtotal">
          <span>小計</span>
        </p>
        <p id="tax">
          <span>税</span>
        </p>
        <p id="total" style="margin-bottom: 30px;">
          <span>合計</span>
        </p>
        <p>
          <span>お預かり金額</span>
          <input id="deposit" name="deposit" type="text">円
        </p>
        <p id="change">
          <span>おつり</span>
        </p>
        <p style="border-bottom: none;width: 60%;">
          <span id="errmsg" style="color: red;"></span>
        </p>
      </div>
      <input type="hidden" name="change" value="">
    </div>
    <footer>
      <%@ include file="footer.jsp" %>
    </footer>
  </form>
</body>
<script>
$(function(){

	subtotal();
	tax();
	total();
	$('#change').append("0円");
	$('.conf').prop('disabled',true);

	// 小計
	function subtotal() {
		let num = <%=sale.getSalePrice()%>;
		$('#subtotal').append(num.toLocaleString() + "円");
	}

	// 税
	function tax() {
		let tax = <%=sale.getTaxIncludingPrice(sale.getSalePrice()) - sale.getSalePrice()%>;
		$('#tax').append(tax.toLocaleString() + "円");
	}

	// 合計
	function total() {
		let total = <%=sale.getTaxIncludingPrice(sale.getSalePrice())%>;
		$('#total').append(total.toLocaleString() + "円");
	}

	// おつり
	function change(deposit) {
		let change = Number(deposit) - <%=sale.getTaxIncludingPrice(sale.getSalePrice())%>;
		$('#change').text("");
		if(change < 0) {
			$('#errmsg').text("お預かり金額が不足しています。");
			$('#change').append("<span>おつり</span>0円");
			$('.conf').prop('disabled',true);
		} else {
			$('#errmsg').text("");
			$('#change').append("<span>おつり</span>" + change.toLocaleString() + "円");
			$('input[name="change"]').val(change);
			$('.conf').prop('disabled',false);
		}
	}

	$('#deposit').change(function() {
		change($(this).val());
	});

});
</script>
</html>