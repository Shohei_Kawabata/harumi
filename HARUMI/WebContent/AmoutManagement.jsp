<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
  <title>コーヒー管理</title>
  <link rel="stylesheet" type="text/css" href="./css/order.css">
</head>
<body>
  <form>
  <%@ include file="header.jsp" %>
  <div class="amount">
    <img src="./image/07_coffee.png" style="width: 40px;">
    <p>コーヒー残量：〇〇〇ℓ  〇〇〇ml</p>
  </div>
  <div class="box" id="makeImg">
    <div class="amount-block">
      <input type="text"><span class="unit" style="margin-right: 20px;">ml</span>
    </div>
    <div class="calc">
      <input class="plus" type="button" value="増やす">
      <input class="minus" type="button" value="減らす">
    </div>
  </div>
  <footer>
    <%@ include file="footer.jsp" %>
  </footer>
  </form>
</body>
</html>