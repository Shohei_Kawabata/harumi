<%@page import="model.entity.Sale"%>
<%@page import="model.entity.Order"%>
<%@page import="java.util.List"%>
<%@page import="java.util.Map"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%
	List<Order> orderList = (List<Order>) session.getAttribute("orders");
	Sale sale = (Sale) session.getAttribute("sale");
	Map<Integer, Integer> moneyType = (Map<Integer, Integer>) session.getAttribute("moneyType");

	int deposit = Integer.parseInt((String) session.getAttribute("deposit"));
	int change = Integer.parseInt((String) session.getAttribute("change"));
%>
<!DOCTYPE html>
<html>
<head>
  <meta charset="UTF-8">
  <title>お支払い</title>
  <link rel="stylesheet" type="text/css" href="./css/order.css">
  <script type="text/javascript" src="./js/jquery-3.4.1.min.js"></script>
</head>
<body>
  <form action="MainMenu" method="post">
  <%@ include file="header.jsp" %>
    <div class="grid-container">
      <div class="result" style="border-right: groove;">
        <p id="deposit">
          <span>お預かり金額</span>
        </p>
        <p id="change">
          <span>おつり</span>
        </p>
        <p>
<%
int[] moneyKinds = {10000, 5000, 1000, 500, 100, 50, 10, 5, 1};
for( int kind : moneyKinds){
	if (moneyType.get(kind) != 0) {
%>
        <span style="text-align: right;display: contents;"><%= kind %>円 : <%= moneyType.get(kind) %>枚</span><br>
<%
	}
}
%>
</p>
      </div>
      <div class="itemlist">
        <p>aaa</p>
      </div>
    </div>
    <footer>
      <%@ include file="resultfooter.jsp" %>
    </footer>
  </form>
</body>
<script>
$(function(){

	deposit();
	change();

	// お預かり金額
	function deposit() {
		let dep = <%= deposit %>;
		$('#deposit').append(dep.toLocaleString() + "円");
	}

	// おつり
	function change() {
		let cg = <%= change %>;
		$('#change').append(cg.toLocaleString() + "円");
	}


});
</script>
</html>