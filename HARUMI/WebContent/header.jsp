<%@page import="model.entity.Employee"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%
Employee empResult = (Employee)session.getAttribute("LoginUserInfo");

String headerUri = request.getServletPath();
String empName = "";

if( empResult == null ){
	empName = "名無しの権兵衛";
} else {
	empName = empResult.getLoginName();
}
%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
  <link rel="stylesheet" type="text/css" href="./css/header.css">
</head>
  <body>
    <header>
      <h2 class="title">
<%if(headerUri.equals("/Order.jsp")){%>
        注文
<%} else if(headerUri.equals("/OrderComfirm.jsp")) {%>
        注文確認
<%} else {%>
        メニュー
<%}%>
      </h2>
      <h4 class="name">
        ログインユーザ：<%= empName %>
      </h4>
    </header>
    <hr>
  </body>
</html>