<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
  <title>メインメニュー</title>
  <link rel="stylesheet" type="text/css" href="./css/menu.css">
  <script type="text/javascript" src="./js/jquery-3.4.1.min.js"></script>
</head>
<body>
  <form action="Logout" method="post">
    <%@ include file="header.jsp" %>
    <div class="parent">
      <input class="order" type="submit" value="注文" formaction="Order">
      <input class="manager" type="submit" value="管理" formaction="AmoutManagement">
      <input class="close" type="submit" value="閉店" formaction="#">
    </div>
    <div class="footer">
      <input class="logout" type="submit" name="logout" value="ログアウト">
      <input class="emp" type="submit" value="従業員管理" formaction="#">
    </div>
  </form>
</body>

<script>
	$(function(){
		// 「管理」ボタンクリック時のイベント
		$('.manager').on('click', function(e) {
			$('.parent').append('<input type="hidden" name="amountFlag" value="1">');
		});
	});
</script>
</html>