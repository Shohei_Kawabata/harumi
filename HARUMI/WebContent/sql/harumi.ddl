CREATE DB harumi USING CODESET UTF-8 TERRITORY JP;
CONNECT TO harumi USER db2inst1;

create table employees
(
	employee_id  INTEGER PRIMARY KEY not null generated always as identity,
	last_name VARCHAR(200),
	first_name VARCHAR(200),
	login_id VARCHAR(100),
	login_password VARCHAR(100),
	admin_flag SMALLINT default
);
create table products
(
	product_id INTEGER PRIMARY KEY not null generated always as identity,
	product_name VARCHAR(100),
	capacity INTEGER,
	price INTEGER
);
create table stocks
(
	stock_id INTEGER PRIMARY KEY not null generated always as identity,
	stock_quantity INTEGER,
	register_time TIMESTAMP 
);

create table sales
(
	sale_id INTEGER PRIMARY KEY not null generated always as identity,
	employee_id  INTEGER,
	sale_price INTEGER,
	sale_time TIMESTAMP
);

create table orders
(
	order_id INTEGER PRIMARY KEY not null generated always as identity,
	sale_id  INTEGER,
	product_id INTEGER,
	quantity INTEGER,
	total_price INTEGER
);
ALTER TABLE orders
	ADD FOREIGN KEY (sale_id)
	REFERENCES sales(sale_id) 
	ON UPDATE RESTRICT
	ON DELETE RESTRICT;
	
ALTER TABLE orders
	ADD FOREIGN KEY (product_id)
	REFERENCES products(product_id) 
	ON UPDATE RESTRICT
	ON DELETE RESTRICT;
	
ALTER TABLE sales
	ADD FOREIGN KEY (employee_id)
	REFERENCES employees(employee_id) 
	ON UPDATE RESTRICT
	ON DELETE RESTRICT;

INSERT INTO employees VALUES(DEFAULT, 'sasaki', 'daisuke', 'sasaki', '3433520', DEFAULT);
connect reset;
	

