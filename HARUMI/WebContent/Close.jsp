<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%
String CLOSE_TEXT = "あ、工藤さん。閉店処理を行いますか？";
%>
<!DOCTYPE html>
<html>
<head>
  <meta charset="UTF-8">
  <title>閉店処理</title>
  <link rel="stylesheet" type="text/css" href="./css/order.css">
  <link rel="stylesheet" type="text/css" href="./css/close.css">
</head>
<body>
  <form action="MainMenu" method="post">
    <%@ include file="header.jsp" %>
    <div class="close">
      <p style="margin-top: 150px;margin-bottom: 50px;"><%= CLOSE_TEXT %></p>
    </div>
    <div class="confirm">
      <input type="submit" value="いいえ">
      <input type="submit" value="はい" formaction="#">
    </div>
  </form>
</body>
</html>