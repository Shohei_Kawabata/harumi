<%@page import="model.entity.Employee"%>
<%@page import="java.util.List"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%
	List<Employee> emps = (List<Employee>)request.getAttribute("employees");
	String msg = (String) request.getAttribute("msg");
	if (msg == null) {
		msg = "";
	}
%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>従業員削除</title>
<style>
table {
  counter-reset: rowCount;
}

table > tbody > tr {
  counter-increment: rowCount;
}

table > tbody > tr > td:first-child::before {
  content: counter(rowCount);
}
</style>
</head>
<body>
	<p><%= msg %></p>
	<form action="EmployeeDelete" method="post">
		<table>
			<thead>
				<tr>
					<th>氏</th>
					<th>名</th>
					<th>ログインID</th>
					<th>削除</th>
				</tr>
			</thead>
			<tbody>
<%
				for (Employee e : emps) {
%>
					<tr>
						<td><%= e.getLastName() %></td>
						<td><%= e.getFirstName() %></td>
						<td><%= e.getLoginName() %></td>
						<td><input type="checkbox" value="<%= e.getId()%>" name="deleteId"></td>
					</tr>
<%	
				}
%>
			</tbody>
		</table>
		<input type="submit" value="削除">
		<input type="reset" value="リセット">
	</form>
</body>
</html>