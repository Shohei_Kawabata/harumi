<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
  <meta charset="UTF-8">
  <title>Login</title>
  <link rel="stylesheet" type="text/css" href="./css/login.css">
</head>
  <body>
    <form action="Login" method="post">
      <h1>ログイン</h1>
      <div class="user-id">
        <input name="user-id" type="text" placeholder="ユーザーID">
      </div>
      <div class="user-pass">
        <input name="password" type="text" placeholder="パスワード">
      </div>
      <div class="login-button">
        <input type="submit" value="ログイン">
      </div>
    </form>
  </body>
</html>