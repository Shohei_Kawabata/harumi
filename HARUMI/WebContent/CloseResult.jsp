<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%
String CLOSE_CONFIRM_TEXT = "閉店処理が完了致しました";
String TANK_INIT_TEXT     = "タンク残量初期化";
%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
  <title>閉店処理</title>
  <link rel="stylesheet" type="text/css" href="./css/order.css">
  <link rel="stylesheet" type="text/css" href="./css/close.css">
</head>
<body>
  <form action="MainMenu" method="post">
    <%@ include file="header.jsp" %>
    <div class="result">
      <p class="close-message"><span><%= CLOSE_CONFIRM_TEXT %></span></p>
      <p class="head-border"><%= TANK_INIT_TEXT %></p>
      <p><span></span></p>
    </div>
  </form>
</body>
</html>