<%@page import="java.util.List"%>
<%@page import="model.entity.Stock"%>
<%@page import="model.entity.Product"%>
<%@page import="java.util.Map"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>

<%
int stockData = (int)request.getAttribute("stockQuantity");
List<Product> productList = (List<Product>) request.getAttribute("productsList");
Map<Product, Integer> map = (Map<Product, Integer>)request.getAttribute("productsMax");//最大買える数取得用

%>
<!DOCTYPE html>
<html>
<head>
  <meta charset="UTF-8">
  <title>注文</title>
  <link rel="stylesheet" type="text/css" href="./css/order.css">
  <script type="text/javascript" src="./js/jquery-3.4.1.min.js"></script>
</head>
<body>
  <form action="Confirm" method="post">
    <%@ include file="header.jsp" %>
    <div class="amount">
      <img src="./image/07_coffee.png" style="width: 40px;">
      <p>コーヒー残量：<span id="coffee"><%= stockData %></span>ml</p><br>
        <div class="itemlist">
<%
  int i = 1;

  for(Product val : productList){
%>
          <p class="item">
            <span><%= val.getName() %></span>
            <span><%= val.getCapacity() %>ml</span>
            <span><%= val.getPrice() %>円</span>
            <button type="button" class="plusbutton" id="plus"><span id="plusbutton"></span></button>
            <input type="text" value="0" id="quantity" name=<%= i %>>
            <button type="button" class="minusbutton" id="minus"><span id="minusbutton"></span></button>
            <input type="hidden" id="cap" value=<%= val.getCapacity() %>>
          </p>
<%
  i++;
  }
%>
        </div>
      <p><span id="errmsg" style="color: red;"></span></p>
      <button type="button" id="testOrder">残量取得</button>
    </div>
    <footer>
      <%@ include file="footer.jsp" %>
    </footer>
  </form>
</body>

<script>
$(function(){;

	// 初期表示
	$("#errmsg").css('visibility', 'hidden');
	$('.conf').prop('disabled',true);

	// プラスボタンクリック時のイベント
	$('.plusbutton').on('click', function() {
		// 次要素(input)を取得
		var click = $(this).next();
		// 要素のvalueを取得して加算
		var calc = click.val();
		calc = Number(calc) + 1;
		click.val(calc);
		// 注文量がコーヒー残量を超過しているか確認
		isComp();
	});

	// マイナスボタンクリック時のイベント
	$('.minusbutton').on('click', function() {
		var click = $(this).prev();
		var calc = click.val();
		if(calc != 0){
			calc = calc - 1;
			click.val(calc);
			// 注文量がコーヒー残量を超過しているか確認
			isComp();
		}
	});

	// コーヒー使用量と残量の比較
	let amount = 0;
	function isComp() {

		// 商品毎のコーヒー量と個数で消費量算出
		for (i = 0; i < caps().length; i++) {
			amount += caps()[i] * quan()[i];
		}

		// 消費量がコーヒー残量を超過した場合、警告を出力し確認ボタンを非活性に
		if(<%= stockData %> <= amount){
			$('#errmsg').css('visibility', 'visible');
			$('#errmsg').text('消費量が、コーヒー残量を超過します。');
			$('.conf').prop('disabled',true);
		} else {
			$('#errmsg').css('visibility', 'hidden');
			$('#errmsg').text('');
			$('.conf').prop('disabled',!isSelectItem(quantity));
		}
	}

	// 商品の選択個数判定関数
	// return : すべて0 = false,1つでも選択 = true;
	function isSelectItem(quantity) {
		let itemFlg = false;

		// 商品数分繰り返し
		for (i = 0; i < quantity.length; i++) {
			if(quantity[i] != 0) {
				itemFlg = true;
			}
		}
		return itemFlg;
	}

	// コーヒー量取得
	function caps(){
		return $('[id=cap]').map(function () {
			return $(this).val();
		});
	}

	//
	function quan(){
		return $('[id=quantity]').map(function () {
			return $(this).val();
		});
	}

	// ajax通信でコーヒー残量更新
	$('#testOrder').on('click', function(){
		sampleAjax();
	})

});

function sampleAjax() {

	  //ajaxでservletにリクエストを送信
	$.ajax({
		type    : "POST",
		url     : "http://localhost:8080/HARUMI/TestOrderServlet",  //送信先のServlet URL（適当に変えて下さい）
		dataType:"json",
		async   : true,           //true:非同期(デフォルト), false:同期

	}).done(function(data1,textStatus,jqXHR) {

		// 1. JavaScriptオブジェクトをJSONに変換
		// stringifyは
		var data2 = JSON.stringify(data1,undefined,2);
		console.dir(JSON.stringify(data1,undefined,2)); //コンソールにJSONが表示される
		$('#coffee').text(data1['response1']);
		console.log($('#coffee').text());


		// 2. failは、通信に失敗した時に実行される
	}).fail(function(jqXHR, textStatus, errorThrown ) {
		console.log(jqXHR.status); //例：404
		console.log(textStatus); //例：error
		console.log(errorThrown); //例：NOT FOUND

		// 3. alwaysは、成功/失敗に関わらず実行される
	}).always(function(){
		console.log('取得終了');

	});

}
</script>
</html>