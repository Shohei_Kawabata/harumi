package model.dao;

import static org.junit.jupiter.api.Assertions.*;

import java.lang.reflect.Method;

import org.junit.jupiter.api.Test;

import model.entity.Employee;

class EmployeeDAOTest {

	@Test	// employee 自体が null
	void testLoginNameCheckEmpisNull() throws Exception {
		EmployeeDAO dao = new EmployeeDAO();
		boolean expected = false;
		Class<?> clazz = dao.getClass();
		Method method = clazz.getDeclaredMethod("loginNameCheck", Employee.class);
		method.setAccessible(true);
		Employee emp = null;
		boolean actual = (boolean) method.invoke(dao, emp);
		assertEquals(expected, actual);
	}
	@Test	// employee の loginNameがnullの場合
	void testLoginNameCheckEmpLoginNameisNull() throws Exception {
		EmployeeDAO dao = new EmployeeDAO();
		boolean expected = false;
		Class<?> clazz = dao.getClass();
		Method method = clazz.getDeclaredMethod("loginNameCheck", Employee.class);
		method.setAccessible(true);
		Employee emp = new Employee();
		boolean actual = (boolean) method.invoke(dao, emp);
		assertEquals(expected, actual);
	}
	@Test	// employee の loginNameが空白文字列の場合
	void testLoginNameCheckEmpLoginNameisBlank() throws Exception {
		EmployeeDAO dao = new EmployeeDAO();
		boolean expected = false;
		Class<?> clazz = dao.getClass();
		Method method = clazz.getDeclaredMethod("loginNameCheck", Employee.class);
		method.setAccessible(true);
		Employee emp = new Employee();
		emp.setLoginName("");
		boolean actual = (boolean) method.invoke(dao, emp);
		assertEquals(expected, actual);
	}
	@Test	// employee の loginNameが文字列"a"がある場合
	void testLoginNameCheckEmpTrue() throws Exception {
		EmployeeDAO dao = new EmployeeDAO();
		boolean expected = true;
		Class<?> clazz = dao.getClass();
		Method method = clazz.getDeclaredMethod("loginNameCheck", Employee.class);
		method.setAccessible(true);
		Employee emp = new Employee();
		emp.setLoginName("a");
		boolean actual = (boolean) method.invoke(dao, emp);
		assertEquals(expected, actual);
	}
}
